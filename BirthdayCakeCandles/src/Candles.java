
    import java.util.Scanner;

    public class Candles {
        int CountCakeCandles() {
            int count = 0;
            Scanner scanner = new Scanner(System.in);
            System.out.println("Introduce the number of candles: ");
            int n = scanner.nextInt();
            int[] candles = new int[n];

            System.out.println("Introduce candles' height: ");
            for (int i = 0; i < n; i++) {
                candles[i] = scanner.nextInt();
            }

            int max = candles[0];
            for (int i = 0; i < n; i++) {
                if (max < candles[i]) max = candles[i];
            }

            for (int i = 0; i < n; i++) {
                if (max == candles[i])
                    count++;
            }
            return count;
        }


        public static void main(String[] args) {
            Candles cake = new Candles();
            System.out.println("The number of candles that are tallest is: " + cake.CountCakeCandles());

        }
    }


